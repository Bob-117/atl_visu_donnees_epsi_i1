# Atelier Visualisation de Données

```shell
git clone && cd
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python main.py
```

## Sujet

Consigne : Faites une analyse comparative entre les ventes mensuelles et la vente moyenne.

Données : Ventes mensuelles de l'article `A21374` de la catégorie `PRE CUTS`.

Sources : Fichier excel fourni par le formateur.

|  January  |  February  |  March  |  April  |  May  |  June  |  July  |  August  |  September  |  October  |  November  |  December  |
|:---------:|:----------:|:-------:|:-------:|:-----:|:------:|:------:|:--------:|:-----------:|:---------:|:----------:|:----------:|
|    17     |     49     |   70    |   110   |  72   |  210   |  110   |   170    |     70      |    22     |     64     |     45     |


## Résultats

### Premiers résultats :

```shell
Basics : 12 items | avg : 84.08333333333333 | std : 57.713175324709056
```

- Nombre d'individus : 12
- Moyenne : 84.083
- Ecart type : 57.713

### Visualisation :

Nous traitons une série de données temporelles (sur 12 mois), ainsi le choix de l'histogramme est adapté aux enjeux induits par l'analyse des chiffres de vente de l'article A21374.

1) Une première visualisation en histogramme permet une meilleure appréciation de l'évolution des chiffres de vente de l'article A21374 au cours d'une année (non précisée dans l'énoncé):

![](asset/hist1.png)

2) Une comparaison à la moyenne permet d'identifier les mois présentant les chiffres de vente de l'article A21374 les plus élevés : Avril, Juin, Juillet & Août 

![](asset/hist2_avg.png)

3) Une comparaison aux quantiles 1 & 3 (les quartiles) permettent d'identifier les mois de Juin et Août comme particulièrement rentables, et les mois de Janvier, Octobre et Décembre comme des mois avec des chiffres de vente très faibles. 

![](asset/hist3_q.png)

4) Nous remarquons que les ventes de l'article suivent une distribution normale, nous pouvons supposer que le modèle présente une périodicité annuelle.

![](asset/hist4_gauss.png)


## Exploitation

Etant donné les résultats et la compréhension des visualisations précédentes, nous conseillons au service marketing de l'entreprise de trouver un nouveau public pour les mois d'hiver,
et de renforcer les mois d'été qui sont déjà des mois présentant de bons chiffres de vente pour l'article `A21374`.

Update (ce sont des consommations et non des volumes ou prix de vente) : 
<br>Les consommations de l'article `A21374` sont fortes en Juin & Août, et très faibles en Janvier, Octobre et Décembre pour cette année (inconnue). Prévoir un réassort de stock (et toute la logistique qui en incombe) pour l'article `A21374`, au printemps en prévision de l'été est une pratique à préconiser auprès de cette entreprise.

## Compléments

Nouvelles consignes (et après avoir compris qu'il s'agit de consommations et non de volumes ou prix de vente):

#### Faire visualiser une courbe comparative des historiques des consommations annuelles à celle de la consommation moyenne

![](asset/ex_2_1_labels.png)

#### Faire visualiser la comparaison des consommations annuelles par rapport a la consommation moyenne

![](asset/ex_2_2_labels.png)

#### Faire visualiser les proportions des 4 trimestres

![](asset/ex_2_3.png)

#### Autres visualisations

![](asset/ex_2_4.png)

![](asset/ex_2_5.png)

![](asset/ex_2_6.png)

## Dropbox :

Forecasting : 

- https://machinelearningmastery.com/time-series-forecasting-methods-in-python-cheat-sheet/
- https://medium.com/analytics-vidhya/predicting-sales-time-series-analysis-forecasting-with-python-b81d3e8ff03f
- https://stackoverflow.com/questions/67629326/predicting-sales-data-with-python
- https://people.duke.edu/~rnau/411arim.htm
- https://machinelearningmastery.com/time-series-forecast-study-python-monthly-sales-french-champagne/
- https://www.geeksforgeeks.org/sales-forecast-prediction-python/
- https://zebrabi.com/guide/how-to-improve-sales-revenue-using-python-matplotlib/
- https://forecastegy.com/posts/sales-forecasting-multiple-products-python/
- https://machinelearningmastery.com/time-series-forecasting-methods-in-python-cheat-sheet/
- https://machinelearningmastery.com/arima-for-time-series-forecasting-with-python/
- https://www.mastersindatascience.org/learning/statistics-data-science/what-is-arima-modeling/


