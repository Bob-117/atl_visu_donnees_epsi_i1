########################################################################################################################
# IMPORT
########################################################################################################################
import matplotlib.pyplot as plt
import numpy as np
import statistics
from scipy.optimize import curve_fit

########################################################################################################################
# DATA
########################################################################################################################
sales = {
    'A20165': [280, 240, 470, 400, 520, 570, 487, 600, 360, 300, 350, 400],
    'A21374': [17, 49, 70, 110, 72, 210, 110, 170, 70, 22, 64, 45]
}

months = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July',
    'August', 'September', 'October', 'November', 'December'
]
months = [m[0:3] for m in months]

ref = 'A21374'
prices = sales[ref]
base_title = f'Sales for Each Month (ref {ref})'


########################################################################################################################
# METHODS
########################################################################################################################
def basic():
    nb_values = len(prices)
    avg = statistics.mean(prices)
    std_dev = statistics.stdev(prices)
    print(f'Basics : {nb_values} items | avg : {avg} | std : {std_dev}')


def hist():
    plt.figure(1)
    plt.bar(months, prices)
    plt.xlabel('Months')
    plt.ylabel('Prices')
    plt.title(f'{base_title}')
    plt.show()


def hist_avg():
    avg = statistics.mean(prices)
    plt.figure(2)
    colors = ['green' if x > avg else 'blue' for x in prices]
    plt.bar(months, prices, color=colors)
    plt.axhline(y=avg, color='r', linestyle='-')
    plt.xlabel('Months')
    plt.ylabel('Prices')
    plt.title(f'{base_title} | Average')
    plt.show()


def hist_quartile():
    q1 = statistics.quantiles(prices, n=4)[0]
    q3 = statistics.quantiles(prices, n=4)[-1]
    colors = ['green' if x > q3 else 'blue' if x > q1 else 'red' for x in prices]
    plt.figure(3)
    plt.bar(months, prices, color=colors)
    plt.axhline(y=q1, color='r', linestyle='--')
    plt.axhline(y=q3, color='g', linestyle='--')
    plt.xlabel('Months')
    plt.ylabel('Prices')
    plt.title(f'{base_title} | 1st & 3rd Quartile')
    plt.show()


# two_years_months = [f'{m[0:3]}{y}' for y in (23, 24) for m in months]
# print(two_years_months)


def gaussian_graph():
    def gaussian(x, amplitude, mean, stddev):
        return amplitude * np.exp(-((x - mean) / stddev) ** 2)

    initial_params = [max(prices), np.mean(prices), np.std(prices)]

    params, covariance = curve_fit(gaussian, range(1, len(prices) + 1), prices, p0=initial_params)

    x_values = np.linspace(1, len(prices), 100)

    plt.scatter(range(1, len(prices) + 1), prices, label='Sales')

    plt.plot(x_values, gaussian(x_values, *params), 'r', label='Fitted Gaussian')

    plt.title(f'{base_title} | Gaussian')
    plt.xlabel('Months')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()


def double_histo():
    monthly_values = prices
    monthly_averages = [np.mean(monthly_values)] * len(monthly_values)
    bar_width = 0.35
    indices = np.arange(len(months))
    fig, ax = plt.subplots()
    bar_values = ax.bar(indices, monthly_values, bar_width, label='Sales')
    bar_averages = ax.bar(indices + bar_width, monthly_averages, bar_width, label='Avg')
    ax.set_xlabel('Months')
    ax.set_ylabel('Sales')
    plt.title(f'{base_title} | Average')
    ax.set_xticks(indices + bar_width / 2)
    ax.set_xticklabels(months)
    ax.legend()
    plt.show()


def exercice_complementaire():
    __prices = [17, 49, 70, 110, 72, 210, 110, 170, 70, 22, 64, 45]
    avg = np.mean(__prices)

    plt.figure(figsize=(10, 5))
    plt.plot(__prices, label='Consommations', marker='o')
    plt.axhline(y=avg, color='r', linestyle='--', label='Consommation Moyenne')
    plt.xlabel('Mois')
    plt.ylabel('Consommation')
    plt.title(f'Comparaison des consommations à la consommation moyenne pour l\'article {ref}')
    plt.xticks(range(len(months)), months)
    plt.legend()
    plt.show()

    plt.figure(figsize=(10, 5))
    diff_par_rapport_moyenne = [cons - avg for cons in __prices]
    bars = plt.bar(range(1, len(__prices) + 1), diff_par_rapport_moyenne, color='b', alpha=0.7)
    plt.axhline(y=0, color='r', linestyle='--', label='Consommation Moyenne')
    plt.xlabel('Mois')
    plt.ylabel('Différence par rapport à la Moyenne')
    plt.title(f'Comparaison des consommations par rapport à la consommation moyenne pour l\'article {ref}')
    plt.legend()
    for bar, month in zip(bars, months):
        yval = bar.get_height()
        plt.text(bar.get_x() + bar.get_width() / 2, yval, month, ha='center', va='bottom', color='black', fontsize=8)
    plt.show()

    trim = ['Hiver', 'Printemps', 'Ete', 'Automne']
    trim_sales = [sum(__prices[i:i + 3]) / sum(__prices) for i in range(0, len(__prices), 3)]
    plt.figure(figsize=(6, 6))
    plt.pie(trim_sales, labels=trim, autopct='%1.1f%%', startangle=90, counterclock=False,
            colors=['#87CEEB', '#00FF00', '#FF0000', '#FFFF00'])
    plt.title(f'Proportions des consommations par trimestre pour l\'article {ref}')
    plt.show()

    monthly_sales_percentage = [__price / sum(__prices) for __price in __prices]
    plt.figure(figsize=(8, 8))
    plt.pie(monthly_sales_percentage, labels=months, autopct='%1.1f%%', startangle=0, counterclock=False)
    plt.title(f'Proportions des consommations par mois pour l\'article {ref}')
    plt.show()

    # quarterly_sales_percentage = [sum(__prices[i:i + 3]) / sum(__prices) for i in range(0, len(__prices), 3)]
    fig, axes = plt.subplots(2, 2, figsize=(10, 10))
    fig.suptitle(f'Proportions des consommations par trimestre pour l\'article {ref}')
    for i, (quarter, ax) in enumerate(zip(trim, axes.flatten())):
        start_index = i * 3
        end_index = start_index + 3
        quarter_sales_percentage = [sale / sum(__prices[start_index:end_index]) for sale in
                                    __prices[start_index:end_index]]
        ax.pie(quarter_sales_percentage, labels=months[start_index:end_index], autopct='%1.1f%%', startangle=90,
               counterclock=False)
        ax.set_title(f'{quarter}')
    plt.show()

    plt.figure(figsize=(12, 6))
    bar_width = 0.35
    index = np.arange(len(months))
    for i, month in enumerate(months):
        plt.bar(index[i] - bar_width / 2, __prices[i], bar_width, label='Consommation', color='skyblue',
                edgecolor='black')
        plt.bar(index[i] + bar_width / 2, avg, bar_width, label='Moyenne', color='orange', edgecolor='black')
    plt.xlabel('Mois')
    plt.ylabel('Consommation')
    plt.title(f'Histogramme de la consommation par mois pour l\'article {ref}')
    plt.xticks(index, months)
    plt.legend()
    plt.show()


########################################################################################################################
# EXECUTION
########################################################################################################################
def process():
    basic()
    hist()
    hist_avg()
    hist_quartile()
    try:
        gaussian_graph()
    except Exception as e:
        print('----------------------------')
        print(f'Cant gauss , u should try another model')
        print(f'Error : \n{e}')
        print('----------------------------')
    exercice_complementaire()
    double_histo()


if __name__ == '__main__':
    process()
